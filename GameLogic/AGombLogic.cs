﻿// <copyright file="AGombLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Logic of The Button.
    /// </summary>
    public class AGombLogic : IAGomb
    {
        private readonly Random r = new Random();

        private string szoveg;
        private char szin;

        /// <summary>
        /// Initializes a new instance of the <see cref="AGombLogic"/> class.
        /// Initializes a new instance of The Button Logic class.
        /// </summary>
        public AGombLogic()
        {
            this.SzinGeneralas(ref this.szin);
            this.SzovegGenerator(ref this.szoveg);
        }

        /// <summary>
        /// Gets or sets color property.
        /// </summary>
        public char Szin
        {
            get { return this.szin; }
            set { this.szin = value; }
        }

        /// <summary>
        /// Gets or sets text property.
        /// </summary>
        public string Szoveg
        {
            get { return this.szoveg; }
            set { this.szoveg = value; }
        }

        /// <inheritdoc/>
        public void SzinGeneralas(ref char valtozo)
        {
            switch (this.r.Next(0, 6))
            {
                case 1:
                    valtozo = 'R';
                    break;
                case 2:
                    valtozo = 'Y';
                    break;
                case 3:
                    valtozo = 'G';
                    break;
                case 4:
                    valtozo = 'B';
                    break;
                case 5:
                    valtozo = 'W';
                    break;
                default:
                    valtozo = 'F';
                    break;
            }
        }

        /// <inheritdoc/>
        public void SzovegGenerator(ref string valtozo)
        {
            switch (this.r.Next(0, 6))
            {
                case 1:
                    valtozo = "Abort";
                    break;
                case 2:
                    valtozo = "Detonate";
                    break;
                case 3:
                    valtozo = "Car";
                    break;
                case 4:
                    valtozo = "Batteries";
                    break;
                case 5:
                    valtozo = "Hold";
                    break;
                default:
                    valtozo = "Press";
                    break;
            }
        }

        /// <inheritdoc/>
        public int Megoldas()
        {
            if (this.szin == 'Y' && this.szoveg == "Abort")
            {
                // duplakatt a gombra
                return 1;
            }
            else if (this.szoveg == "Detonate")
            {
                // sima katt a gombra
                return 2;
            }
            else if (this.szin == 'B' && this.szoveg == "Car")
            {
                return 1;
            }
            else if (this.szoveg == "Batteries")
            {
                return 2;
            }
            else if (this.szin == 'F')
            {
                return 1;
            }
            else if (this.szin == 'W' && this.szoveg == "Hold")
            {
                // jobb klikk a gombra
                return 3;
            }
            else
            {
                return 1;
            }
        }

        /// <inheritdoc/>
        public string GombFelengedes()
        {
            if (this.szin == 'Y')
            {
                return "4";
            }
            else if (this.szin == 'B')
            {
                return "1";
            }
            else if (this.szin == 'F')
            {
                return "5";
            }
            else
            {
                return "2";
            }
        }
    }
}
