﻿// <copyright file="IVezetekekLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface of wires module.
    /// </summary>
    internal interface IVezetekekLogic
    {
        /// <summary>
        /// generates the wires.
        /// </summary>
        /// <param name="tarolo">that's where the wires will be saved.</param>
        void Generalas(ref char[] tarolo);

        /// <summary>
        /// searches the x-th wires.
        /// </summary>
        /// <param name="x">number of what place should be the wire.</param>
        /// <returns>place where the x-th wire is.</returns>
        int IdxKeres(int x);

        /// <summary>
        /// searches the last place for a wire which has the searched color.
        /// </summary>
        /// <param name="szin">color.</param>
        /// <returns>the index of the last wire, which has the searched color.</returns>
        int UtolsoSzin(char szin);

        /// <summary>
        /// searches in the serial number by place.
        /// </summary>
        /// <param name="place">place to search.</param>
        /// <param name="search">which color it should be.</param>
        /// <returns>true, if there is a correct colored wire in the searched place, false if there is not.</returns>
        bool SNPlaceSearch(int place, char search);

        /// <summary>
        /// Searches the last wire's color.
        /// </summary>
        /// <returns>last wire's color.</returns>
        char Utolso();

        /// <summary>
        /// Makes the solution of the module.
        /// </summary>
        /// <returns>solution of the module.</returns>
        int Megoldas();
    }
}
