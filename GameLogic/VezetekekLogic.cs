﻿// <copyright file="VezetekekLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// logic of wires module.
    /// </summary>
    public class VezetekekLogic : IVezetekekLogic
    {
        private readonly string sn;
        private readonly char utolso;
        private char[] vezetekek;

        private int utolsoidx;

        private int uresek = 0;

        private int feher = 0;
        private int piros = 0;

        private int zold = 0;

        private int sarga = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="VezetekekLogic"/> class.
        /// </summary>
        /// <param name="sN">serial number.</param>
        public VezetekekLogic(string sN)
        {
            this.Fekete = 0;
            this.Kek = 0;
            this.vezetekek = new char[6];
            this.sn = sN;
            this.Generalas(ref this.vezetekek);
            this.utolso = this.Utolso();
        }

        /// <summary>
        /// gets or sets the wires.
        /// </summary>
        public char[] Vezetekek
        {
            get { return this.vezetekek; }
            set { this.vezetekek = value; }
        }

        /// <summary>
        /// gets or sets number of black wires.
        /// </summary>
        public int Fekete { get; set; }

        /// <summary>
        /// gets or sets number of blue wires.
        /// </summary>
        public int Kek { get; set; }

        /// <inheritdoc/>
        public void Generalas(ref char[] tarolo)
        {
            Random r = new Random();

            for (int i = 0; i < 6; i++)
            {
                if (this.uresek != 3)
                {
                    if (r.Next(3) == 0)
                    {
                        this.uresek++;
                        tarolo[i] = 'Ü';
                    }
                    else
                    {
                        switch (r.Next(5))
                        {
                            case 0:
                                tarolo[i] = 'F';
                                this.Fekete++;
                                break;
                            case 1:
                                tarolo[i] = 'P';
                                this.piros++;
                                break;
                            case 2:
                                tarolo[i] = 'W';
                                this.feher++;
                                break;
                            case 3:
                                tarolo[i] = 'S';
                                this.sarga++;
                                break;
                            case 4:
                                tarolo[i] = 'K';
                                this.Kek++;
                                break;
                            default:
                                tarolo[i] = 'Z';
                                this.zold++;
                                break;
                        }
                    }
                }
                else
                {
                    switch (r.Next(5))
                    {
                        case 0:
                            tarolo[i] = 'F';
                            this.Fekete++;
                            break;
                        case 1:
                            tarolo[i] = 'P';
                            this.piros++;
                            break;
                        case 2:
                            tarolo[i] = 'W';
                            this.feher++;
                            break;
                        case 3:
                            tarolo[i] = 'S';
                            this.sarga++;
                            break;
                        case 4:
                            tarolo[i] = 'K';
                            this.Kek++;
                            break;
                        default:
                            tarolo[i] = 'Z';
                            this.zold++;
                            break;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public int Megoldas()
        {
            if (this.uresek == 3)
            {
                if (this.feher == 0)
                {
                    return this.IdxKeres(2);
                }
                else if (this.utolso == 'K')
                {
                    return this.utolsoidx;
                }
                else if (this.sarga > 1)
                {
                    return this.UtolsoSzin('S');
                }
                else
                {
                    return this.IdxKeres(3); // utolsó vezeték
                }
            }
            else if (this.uresek == 2)
            {
                if (this.feher > 1 && (this.SNPlaceSearch(5, '1') || this.SNPlaceSearch(5, '3') || this.SNPlaceSearch(5, '5') || this.SNPlaceSearch(5, '7') || this.SNPlaceSearch(5, '9')))
                {
                    return this.UtolsoSzin('W');
                }
                else if (this.utolso == 'F' && this.feher == 0)
                {
                    return this.IdxKeres(1);
                }
                else if (this.sarga == 1)
                {
                    return this.IdxKeres(1);
                }
                else if (this.Fekete > 1)
                {
                    return this.utolsoidx;
                }
                else
                {
                    return this.IdxKeres(2);
                }
            }
            else if (this.uresek == 1)
            {
                if (this.utolso == 'Z' && (this.SNPlaceSearch(5, '1') || this.SNPlaceSearch(5, '3') || this.SNPlaceSearch(5, '5') || this.SNPlaceSearch(5, '7') || this.SNPlaceSearch(5, '9')))
                {
                    return this.IdxKeres(4);
                }
                else if (this.feher == 1 && this.Fekete > 1)
                {
                    return this.IdxKeres(1);
                }
                else if (this.zold == 0)
                {
                    return this.IdxKeres(2);
                }
                else
                {
                    return this.IdxKeres(1);
                }
            }
            else
            {
                if (this.Fekete == 0 && (this.SNPlaceSearch(5, '1') || this.SNPlaceSearch(5, '3') || this.SNPlaceSearch(5, '5') || this.SNPlaceSearch(5, '7') || this.SNPlaceSearch(5, '9')))
                {
                    return this.IdxKeres(3);
                }
                else if (this.Fekete == 1 && this.Kek > 1)
                {
                    return this.IdxKeres(4);
                }
                else if (this.feher == 0)
                {
                    return this.utolsoidx;
                }
                else
                {
                    return this.IdxKeres(4);
                }
            }
        }

        /// <inheritdoc/>
        public int IdxKeres(int x)
        {
            int vezetek = 0;

            for (int i = 0; i < this.utolsoidx + 1; i++)
            {
                if (this.vezetekek[i] != 'Ü')
                {
                    vezetek++;
                    if (vezetek == x)
                    {
                        return i;
                    }
                }
            }

            return 0;
        }

        /// <inheritdoc/>
        public int UtolsoSzin(char szin)
        {
            int vissza = 0;
            for (int i = 0; i < this.utolsoidx + 1; i++)
            {
                if (this.vezetekek[i] == szin)
                {
                    vissza = i;
                }
            }

            return vissza;
        }

        /// <inheritdoc/>
        public bool SNPlaceSearch(int place, char search)
        {
            return this.sn[place] == search;
        }

        /// <inheritdoc/>
        public char Utolso()
        {
            int i = 5;
            while (i >= 0)
            {
                if (this.vezetekek[i] == 'Ü')
                {
                    i--;
                }
                else
                {
                    this.utolsoidx = i;
                    return this.vezetekek[i];
                }
            }

            return 'Ü';
        }
    }
}
