﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using GameModel;

    /// <summary>
    /// Main logic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private readonly Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">gamemodel.</param>
        public GameLogic(Model model)
        {
            this.Hibak = 0;
            this.model = model;
            this.Sn = this.SerialNumberGenerator();

            this.Vl = new VezetekekLogic(this.SN);

            this.Bgl = new BetusGombokLogic();
            this.BetusGombLenyomasSzama = 0;

            this.Sml = new SimonMondjaLogic(this.Maganhangzo());
            this.SMLevel = 1;

            this.Ml = new MemoriaLogic();
            this.MemoriaLevel = 1;

            this.Agl = new AGombLogic();

            this.Megoldasok();

            this.Vezetekek = this.Vl.Vezetekek;
            this.Betuk = this.Bgl.Gombok;
            this.Ml.KijelzoGenerator();

            this.HS = new HighScore[10];
            this.HsNumber = 0;
        }

        /// <summary>
        /// refresh of screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <summary>
        /// Gets or sets High score.
        /// </summary>
        public HighScore[] HS { get; set; }

        /// <summary>
        /// Gets screen for Memory module.
        /// </summary>
        public int Kijelzo
        {
            get { return this.Ml.Kijelzo; }
        }

        /// <summary>
        /// Gets Solution for The Button module.
        /// </summary>
        public int AGombMegoldas { get; private set; }

        /// <summary>
        /// Gets color of The Button.
        /// </summary>
        public char AGombSzin
        {
            get
            {
                return this.Agl.Szin;
            }
        } // A Gomb színe

        /// <summary>
        /// Gets text of The Button.
        /// </summary>
        public string AGombSzoveg
        {
            get
            {
                return this.Agl.Szoveg;
            }
        } // A Gombon szereplő szöveg

        /// <summary>
        /// Gets Serial number.
        /// </summary>
        public string SN
        {
            get { return this.Sn; }
        }

        /// <summary>
        /// Gets number of errors.
        /// </summary>
        public int Hibak { get; private set; }

        /// <summary>
        /// Gets buttons for memory module.
        /// </summary>
        public int[] MemoriaGombok
        {
            get
            {
                switch (this.MemoriaLevel)
                {
                    case 2:
                        return this.Ml.Gombok2;
                    case 3:
                        return this.Ml.Gombok3;
                    case 4:
                        return this.Ml.Gombok4;
                    case 5:
                        return this.Ml.Gombok5;
                    default:
                        return this.Ml.Gombok1;
                }
            }
        }

        /// <summary>
        /// Gets colors for simon says module.
        /// </summary>
        public string SimonMondjaSzinek
        {
            get
            {
                if (this.SMLevel == 1)
                {
                    return this.Sml.Szinek.Substring(0, 3);
                }
                else if (this.SMLevel == 2)
                {
                    return this.Sml.Szinek.Substring(0, 4);
                }
                else
                {
                    return this.Sml.Szinek;
                }
            }
        } // innen lehet lekérdezni a megvilágítandó színeket

        /// <summary>
        /// Gets or sets wires for wires module.
        /// </summary>
        public char[] Vezetekek { get; set; }

        /// <summary>
        /// Gets or sets letters for Lettered Buttons module.
        /// </summary>
        public char[] Betuk { get; set; }

        /// <summary>
        /// Gets or sets Solution for wires module.
        /// </summary>
        public int VezetekekMegoldas { get; set; }

        /// <summary>
        /// Gets or sets Wires logic.
        /// </summary>
        public VezetekekLogic Vl { get; set; }

        /// <summary>
        /// Gets or sets solution for lettered buttons module.
        /// </summary>
        public string BetusGombokMegoldas { get; set; }

        /// <summary>
        /// Gets or sets number of pushing for lettered buttons module.
        /// </summary>
        public int BetusGombLenyomasSzama { get; set; }

        /// <summary>
        /// Gets or sets Lettered buttons logic.
        /// </summary>
        public BetusGombokLogic Bgl { get; set; }

        /// <summary>
        /// Gets or sets simon says logic.
        /// </summary>
        public SimonMondjaLogic Sml { get; set; }

        /// <summary>
        /// gets or sets level of simon says module.
        /// </summary>
        public int SMLevel { get; set; }

        /// <summary>
        /// gets or sets memory logic.
        /// </summary>
        public MemoriaLogic Ml { get; set; }

        /// <summary>
        /// gets or sets level of memory module.
        /// </summary>
        public int MemoriaLevel { get; set; }

        /// <summary>
        /// gets or sets the button logic.
        /// </summary>
        public AGombLogic Agl { get; set; }

        /// <summary>
        /// gets or sets serial number.
        /// </summary>
        public string Sn { get; set; }

        /// <summary>
        /// gets or sets number of high scores.
        /// </summary>
        public int HsNumber { get; set; }

        /// <inheritdoc/>
        public bool MemoriaKesz()
        {
            if (this.MemoriaLevel == 6)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool MemoriaEllenorzes(int tipp)
        {
            if (tipp == this.Ml.MegoldasHandler(this.MemoriaLevel))
            {
                this.MemoriaLevel++;
                this.Ml.KijelzoGenerator();
                return true;
            }
            else
            {
                this.HibaNovelo();
                this.MemoriaLevel = 1;
                this.Ml.KijelzoGenerator();
                return false;
            }
        }

        /// <inheritdoc/>
        public int[] MemoriaAktualisGombok()
        {
            if (this.MemoriaLevel == 1)
            {
                return this.Ml.Gombok1;
            }
            else if (this.MemoriaLevel == 2)
            {
                return this.Ml.Gombok2;
            }
            else if (this.MemoriaLevel == 3)
            {
                return this.Ml.Gombok3;
            }
            else if (this.MemoriaLevel == 4)
            {
                return this.Ml.Gombok4;
            }
            else
            {
                return this.Ml.Gombok5;
            }
        }

        /// <inheritdoc/>
        public bool AGombEllenorzes()
        {
            if (this.model.OraModul_HatralevoIdo.ToString().Contains(this.Agl.GombFelengedes()))
            {
                return true;
            }

            this.HibaNovelo();
            return false;
        }

        /// <inheritdoc/>
        public bool SimonMondjaKesz()
        {
            if (this.SMLevel == 4)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool SimonMondjaEllenorzes(string tipp)
        {
            if (this.Hibak == 0)
            {
                if (this.SMLevel == 1)
                {
                    if (tipp == this.Sml.Megoldas0.Substring(0, 3))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else if (this.SMLevel == 2)
                {
                    if (tipp == this.Sml.Megoldas0.Substring(0, 4))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else
                {
                    if (tipp == this.Sml.Megoldas0)
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
            }
            else if (this.Hibak == 1)
            {
                if (this.SMLevel == 1)
                {
                    if (tipp == this.Sml.Megoldas1.Substring(0, 3))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else if (this.SMLevel == 2)
                {
                    if (tipp == this.Sml.Megoldas1.Substring(0, 4))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else
                {
                    if (tipp == this.Sml.Megoldas1)
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
            }
            else
            {
                if (this.SMLevel == 1)
                {
                    if (tipp == this.Sml.Megoldas2.Substring(0, 3))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else if (this.SMLevel == 2)
                {
                    if (tipp == this.Sml.Megoldas2.Substring(0, 4))
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
                else
                {
                    if (tipp == this.Sml.Megoldas2)
                    {
                        this.SMLevel++;
                        return true;
                    }

                    this.HibaNovelo();
                    return false;
                }
            }
        }

        /// <inheritdoc/>
        public bool BetusGombokEllenorzes(int tipp)
        {
            if (tipp.ToString() == this.BetusGombokMegoldas[this.BetusGombLenyomasSzama].ToString())
            {
                this.BetusGombLenyomasSzama++;
                return true;
            }
            else
            {
                this.BetusGombLenyomasSzama = 0;
                this.HibaNovelo();
                return false;
            }
        }

        /// <inheritdoc/>
        public bool VezetekekEllenorzes(int tipp)
        {
            if (tipp == this.VezetekekMegoldas)
            {
                return true;
            }

            this.HibaNovelo();
            return false;
        }

        /// <inheritdoc/>
        public void HibaNovelo()
        {
            this.Hibak++;
            if (this.Hibak == 2)
            {
                // felrobban
            }
        }

        /// <inheritdoc/>
        public void Megoldasok()
        {
            this.VezetekekMegoldas = this.Vl.Megoldas();

            this.BetusGombokMegoldas = this.Bgl.Megoldas();

            this.AGombMegoldas = this.Agl.Megoldas();
        }

        /// <inheritdoc/>
        public string SerialNumber()
        {
            return this.SN;
        }

        /// <inheritdoc/>
        public bool Maganhangzo()
        {
            return this.SNSearch('A') || this.SNSearch('E') || this.SNSearch('I') || this.SNSearch('O') || this.SNSearch('U');
        }

        /// <inheritdoc/>
        public void IdozitoCsokkenes()
        {
            if (this.model.OraModul_HatralevoIdo != 0)
            {
                if (this.model.VezetekekModul_IsDone && this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone)
                {
                    this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    this.model.OraModul_HatralevoIdo--;
                    this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public void HighScoreSave(string name)
        {
            HighScore current = this.HighScoreGenerator(name);

            // top 10 eltárolva, sorrendben(hibák száma, hátralévő idő)
            // nincs teli --> fixen bekerül
            // teli van --> lehet bekerül, lehet nem
            int helyezes = 0;
            bool okay = false;

            foreach (HighScore item in this.HS)
            {
                if (!okay)
                {
                    if (item.Hibak >= current.Hibak)
                    {
                        if (((item.Hibak == current.Hibak) && (item.HatralevoIdo < current.HatralevoIdo)) || (item.Hibak > current.Hibak))
                        {
                            okay = true;
                        }
                        else
                        {
                            helyezes++;
                        }
                    }
                    else
                    {
                        helyezes++;
                    }
                }
            }

            if (helyezes < 10)
            {
                for (int i = helyezes; i < this.HS.Length; i++)
                {
                    HighScore seged = this.HS[i];
                    this.HS[i] = current;
                    current = seged;
                }

                string workingDirectory = Environment.CurrentDirectory;
                StreamWriter sw = new StreamWriter(Directory.GetParent(workingDirectory).Parent.FullName + @"/HighScores/highscores.txt", false);

                for (int i = 0; i < this.HS.Length; i++)
                {
                    sw.WriteLine(this.HS[i].Name + "," + this.HS[i].Hibak + "," + this.HS[i].HatralevoIdo);
                }

                sw.Close();
            }
        }

        /// <inheritdoc/>
        public void HighScoreLoad()
        {
            string workingDirectory = Environment.CurrentDirectory;
            string[] fajl = File.ReadAllLines(Directory.GetParent(workingDirectory).Parent.FullName + @"/HighScores/highscores.txt");

            for (int i = 0; i < fajl.Length; i++)
            {
                this.HS[this.HsNumber++] = new HighScore
                {
                    Name = fajl[i].Split(',')[0],
                    Hibak = int.Parse(fajl[i].Split(',')[1]),
                    HatralevoIdo = int.Parse(fajl[i].Split(',')[2]),
                };
            }
        }

        private string SerialNumberGenerator()
        {
            return this.SNAlpha(3) + this.SNNum(3);
        }

        private string SNAlpha(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private string SNNum(int length)
        {
            Random random = new Random();
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private bool SNSearch(char search)
        {
            foreach (char c in this.SN)
            {
                if (c == search)
                {
                    return true;
                }
            }

            return false;
        }

        private HighScore HighScoreGenerator(string name)
        {
            HighScore current = new HighScore()
            {
                Name = name,
                Hibak = this.Hibak,
                HatralevoIdo = this.model.OraModul_HatralevoIdo,
            };

            return current;
        }
    }
}
