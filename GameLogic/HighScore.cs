﻿// <copyright file="HighScore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// One row of high score.
    /// </summary>
    public class HighScore
    {
        private string name;

        private int hibak;

        private int hatralevoIdo;

        /// <summary>
        /// Gets or sets remaining time.
        /// </summary>
        public int HatralevoIdo
        {
            get { return this.hatralevoIdo; }
            set { this.hatralevoIdo = value; }
        }

        /// <summary>
        /// gets or sets number of errors.
        /// </summary>
        public int Hibak
        {
            get { return this.hibak; }
            set { this.hibak = value; }
        }

        /// <summary>
        /// gets or sets player name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
    }
}
