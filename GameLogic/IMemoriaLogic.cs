﻿// <copyright file="IMemoriaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface of memory logic.
    /// </summary>
    internal interface IMemoriaLogic
    {
        /// <summary>
        /// generates the display of the module.
        /// </summary>
        void KijelzoGenerator();

        /// <summary>
        /// generates the numbers on the buttons.
        /// </summary>
        /// <returns>numbers on the buttons.</returns>
        int[] RandomGeneralas();

        /// <summary>
        /// Handling the levels of the module.
        /// </summary>
        /// <param name="lvl">level.</param>
        /// <returns>Solution of the current level.</returns>
        int MegoldasHandler(int lvl);
    }
}
