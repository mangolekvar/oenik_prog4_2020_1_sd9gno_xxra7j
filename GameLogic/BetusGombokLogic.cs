﻿// <copyright file="BetusGombokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Logic of Lettered Buttons.
    /// </summary>
    public class BetusGombokLogic : IBetusGombokLogic
    {
        private readonly string[] tabla;
        private char[] gombok;
        private int[] sorok;

        /// <summary>
        /// Initializes a new instance of the <see cref="BetusGombokLogic"/> class.
        /// </summary>
        public BetusGombokLogic()
        {
            this.tabla = new string[] { "đŁ¤^ß{#", "$đ#*ł{?", "ä÷*Đ~¤ł", "×|&ßĐ?¸", "§¸&°|]@", "×$%Ä§>=" };
            this.gombok = new char[4];
            this.Generalas(ref this.gombok);
        }

        /// <summary>
        /// Gets or sets buttons.
        /// </summary>
        public char[] Gombok
        {
            get { return this.gombok; }
            set { this.gombok = value; }
        }

        /// <summary>
        /// Gets or sets rows.
        /// </summary>
        public int[] Sorok
        {
            get { return this.sorok; }
            set { this.sorok = value; }
        }

        /// <inheritdoc/>
        public void Generalas(ref char[] tarolo)
        {
            Random r = new Random();
            string oszlop = this.tabla[r.Next(6)];
            this.Sorok = this.RandomGeneralas();

            for (int i = 0; i < tarolo.Length; i++)
            {
                tarolo[i] = oszlop[this.Sorok[i]];
            }
        }

        /// <inheritdoc/>
        public int[] RandomGeneralas()
        {
            Random r = new Random();
            int done = 0;
            int[] generalt = new int[4];

            int n = r.Next(7);

            while (done != 4)
            {
                if (done == 0)
                {
                    generalt[done++] = n;
                }
                else if (done == 1)
                {
                    while (n == generalt[0])
                    {
                        n = r.Next(7);
                    }

                    generalt[done++] = n;
                }
                else if (done == 2)
                {
                    while (n == generalt[0] || n == generalt[1])
                    {
                        n = r.Next(7);
                    }

                    generalt[done++] = n;
                }
                else if (done == 3)
                {
                    while (n == generalt[0] || n == generalt[1] || n == generalt[2])
                    {
                        n = r.Next(7);
                    }

                    generalt[done++] = n;
                }
            }

            return generalt;
        }

        /// <inheritdoc/>
        public string Megoldas()
        {
            int min = 0;
            int max = 0;
            int masodikmin = 1;
            int harmadikmin = 2;

            for (int i = 1; i < this.Sorok.Length; i++)
            {
                if (this.Sorok[i] < this.Sorok[min])
                {
                    harmadikmin = masodikmin;
                    masodikmin = min;
                    min = i;
                }
                else if (this.Sorok[i] < this.Sorok[masodikmin])
                {
                    harmadikmin = masodikmin;
                    masodikmin = i;
                }
                else if (this.Sorok[i] > this.Sorok[max])
                {
                    max = i;
                }
                else
                {
                    harmadikmin = i;
                }
            }

            return this.Konvertalas(++min, ++masodikmin, ++harmadikmin, ++max);
        }

        /// <inheritdoc/>
        public string Konvertalas(int a, int b, int c, int d)
        {
            return a.ToString() + b.ToString() + c.ToString() + d.ToString();
        }
    }
}
