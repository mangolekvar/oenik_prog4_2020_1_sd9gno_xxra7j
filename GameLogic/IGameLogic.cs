﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface of main logic.
    /// </summary>
    internal interface IGameLogic
    {
        /// <summary>
        /// Handler of memory module.
        /// </summary>
        /// <returns>true if the module is correctly done, or false if it is not.</returns>
        bool MemoriaKesz();

        /// <summary>
        /// Checks the answer for memory module.
        /// </summary>
        /// <param name="tipp">answer.</param>
        /// <returns>true if the answer is correct, false if it is not.</returns>
        bool MemoriaEllenorzes(int tipp);

        /// <summary>
        /// Access to the buttons of Memory module.
        /// </summary>
        /// <returns>buttons of the current level of memory module.</returns>
        int[] MemoriaAktualisGombok();

        /// <summary>
        /// Checks the answer for the button module.
        /// </summary>
        /// <returns>true if it is correctly done, false if it is not.</returns>
        bool AGombEllenorzes();

        /// <summary>
        /// Handler of simon says module.
        /// </summary>
        /// <returns>true fóif it is corr3ectly done, false if it is not.</returns>
        bool SimonMondjaKesz();

        /// <summary>
        /// checks the answer of simon says module.
        /// </summary>
        /// <param name="tipp">answer.</param>
        /// <returns>true if the answer is correct, false if it is not.</returns>
        bool SimonMondjaEllenorzes(string tipp);

        /// <summary>
        /// Checks the answer of leterred buttons module.
        /// </summary>
        /// <param name="tipp">answer.</param>
        /// <returns>true if the answer is correct, false if it is not.</returns>
        bool BetusGombokEllenorzes(int tipp);

        /// <summary>
        /// Checks the answer of wired module.
        /// </summary>
        /// <param name="tipp">answer.</param>
        /// <returns>true if the answer is correct, false if it is not.</returns>
        bool VezetekekEllenorzes(int tipp);

        /// <summary>
        /// Summary of solutions of the modules.
        /// </summary>
        void Megoldasok();

        /// <summary>
        /// returns the serial number of the bomb.
        /// </summary>
        /// <returns>Serial number of the bomb.</returns>
        string SerialNumber();

        /// <summary>
        /// checks if there's a vowel in the bomb's serial number.
        /// </summary>
        /// <returns>true if there is a vowel in the bomb's serial number, false if there is not.</returns>
        bool Maganhangzo();

        /// <summary>
        /// Decreases the value of remaining time.
        /// </summary>
        void IdozitoCsokkenes();

        /// <summary>
        /// increases the number of errors made.
        /// </summary>
        void HibaNovelo();

        /// <summary>
        /// saves the new high score ranking.
        /// </summary>
        /// <param name="name">player name.</param>
        void HighScoreSave(string name);

        /// <summary>
        /// loads the high score ranking.
        /// </summary>
        void HighScoreLoad();
    }
}
