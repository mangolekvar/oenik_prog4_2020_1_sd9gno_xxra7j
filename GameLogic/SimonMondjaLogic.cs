﻿// <copyright file="SimonMondjaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// logic of simon says module.
    /// </summary>
    public class SimonMondjaLogic : ISimonMondjaLogic
    {
        private readonly Random r;
        private string szinek;

        private string megoldas0;
        private string megoldas1;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimonMondjaLogic"/> class.
        /// </summary>
        /// <param name="mg">has the serial number vowel?.</param>
        public SimonMondjaLogic(bool mg)
        {
            this.szinek = string.Empty;
            this.megoldas0 = string.Empty;
            this.megoldas1 = string.Empty;
            this.Megoldas2 = string.Empty;
            this.r = new Random();
            this.Inditas(mg);
        }

        /// <summary>
        /// gets or sets last solution.
        /// </summary>
        public string Megoldas2 { get; set; }

        /// <summary>
        /// gets second solution.
        /// </summary>
        public string Megoldas1
        {
            get { return this.megoldas1; }
        }

        /// <summary>
        /// gets or sets first solution.
        /// </summary>
        public string Megoldas0
        {
            get { return this.megoldas0; }
            set { this.megoldas0 = value; }
        }

        /// <summary>
        /// gets or sets colors.
        /// </summary>
        public string Szinek
        {
            get { return this.szinek; }
            set { this.szinek = value; }
        }

        /// <inheritdoc/>
        public void Inditas(bool mg)
        {
            if (mg)
            {
                for (int i = 0; i < 5; i++)
                {
                    this.SzinGeneralasMaganhangzo();
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    this.SzinGeneralasNoMaganhangzo();
                }
            }
        }

        /// <inheritdoc/>
        public void SzinGeneralasMaganhangzo()
        {
            switch (this.r.Next(1, 5))
            {
                case 1:
                    this.szinek += "Y";
                    this.megoldas0 += "R";
                    this.megoldas1 += "G";
                    this.Megoldas2 += "R";
                    break;
                case 2:
                    this.szinek += "B";
                    this.megoldas0 += "G";
                    this.megoldas1 += "R";
                    this.Megoldas2 += "B";
                    break;
                case 3:
                    this.szinek += "R";
                    this.megoldas0 += "Y";
                    this.megoldas1 += "B";
                    this.Megoldas2 += "Y";
                    break;
                default:
                    this.szinek += "G";
                    this.megoldas0 += "B";
                    this.megoldas1 += "Y";
                    this.Megoldas2 += "G";
                    break;
            }
        }

        /// <inheritdoc/>
        public void SzinGeneralasNoMaganhangzo()
        {
            switch (this.r.Next(1, 5))
            {
                case 1:
                    this.szinek += "Y";
                    this.megoldas0 += "Y";
                    this.megoldas1 += "B";
                    this.Megoldas2 += "G";
                    break;
                case 2:
                    this.szinek += "B";
                    this.megoldas0 += "R";
                    this.megoldas1 += "G";
                    this.Megoldas2 += "R";
                    break;
                case 3:
                    this.szinek += "R";
                    this.megoldas0 += "G";
                    this.megoldas1 += "Y";
                    this.Megoldas2 += "B";
                    break;
                default:
                    this.szinek += "G";
                    this.megoldas0 += "B";
                    this.megoldas1 += "R";
                    this.Megoldas2 += "Y";
                    break;
            }
        }
    }
}
