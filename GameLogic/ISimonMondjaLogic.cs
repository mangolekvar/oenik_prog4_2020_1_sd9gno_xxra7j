﻿// <copyright file="ISimonMondjaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface of simon says module.
    /// </summary>
    internal interface ISimonMondjaLogic
    {
        /// <summary>
        /// Starts the module first time.
        /// </summary>
        /// <param name="mg">asnwer of "Is there any vowel in the serial nuber?".</param>
        void Inditas(bool mg);

        /// <summary>
        /// Generates the colors when there is any vowel in the serial number.
        /// </summary>
        void SzinGeneralasMaganhangzo();

        /// <summary>
        /// Generates the colors when there is not any vowel in the serial number.
        /// </summary>
        void SzinGeneralasNoMaganhangzo();
    }
}
