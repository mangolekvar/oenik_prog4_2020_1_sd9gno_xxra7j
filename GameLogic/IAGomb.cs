﻿// <copyright file="IAGomb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface for the button module.
    /// </summary>
    internal interface IAGomb
    {
        /// <summary>
        /// Generates the color of the button.
        /// </summary>
        /// <param name="valtozo">that's where the result will go.</param>
        void SzinGeneralas(ref char valtozo);

        /// <summary>
        /// Generates the text of the button.
        /// </summary>
        /// <param name="valtozo">that's where the result will go.</param>
        void SzovegGenerator(ref string valtozo);

        /// <summary>
        /// Solution for module.
        /// </summary>
        /// <returns>how the player should click on the button.</returns>
        int Megoldas();

        /// <summary>
        /// Solution for module.
        /// </summary>
        /// <returns>which number should be displayed on the bomb, when the button clicked.</returns>
        string GombFelengedes();
    }
}
