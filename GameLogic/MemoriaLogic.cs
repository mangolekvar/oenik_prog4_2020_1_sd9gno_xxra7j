﻿// <copyright file="MemoriaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Logic of memory module.
    /// </summary>
    public class MemoriaLogic : IMemoriaLogic
    {
        private readonly Random r;
        private int[] megoldasok;

        private int kijelzo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoriaLogic"/> class.
        /// </summary>
        public MemoriaLogic()
        {
            this.r = new Random();
            this.Gombok1 = this.RandomGeneralas();
            this.Gombok2 = this.RandomGeneralas();
            this.Gombok3 = this.RandomGeneralas();
            this.Gombok4 = this.RandomGeneralas();
            this.Gombok5 = this.RandomGeneralas();
            this.megoldasok = new int[5];
        }

        /// <summary>
        /// Gets screen.
        /// </summary>
        public int Kijelzo
        {
            get { return this.kijelzo; }
        }

        /// <summary>
        /// gets or sets the first level's buttons.
        /// </summary>
        public int[] Gombok1 { get; set; }

        /// <summary>
        /// gets or sets the second level's buttons.
        /// </summary>
        public int[] Gombok2 { get; set; }

        /// <summary>
        /// gets or sets the third level's buttons.
        /// </summary>
        public int[] Gombok3 { get; set; }

        /// <summary>
        /// gets or sets the fourth level's buttons.
        /// </summary>
        public int[] Gombok4 { get; set; }

        /// <summary>
        /// gets or sets the last level's buttons.
        /// </summary>
        public int[] Gombok5 { get; set; }

        /// <inheritdoc/>
        public void KijelzoGenerator()
        {
            this.kijelzo = this.r.Next(1, 5);
        }

        /// <inheritdoc/>
        public int[] RandomGeneralas()
        {
            int done = 0;
            int[] generalt = new int[4];

            int n = this.r.Next(1, 5);

            while (done != 4)
            {
                if (done == 0)
                {
                    generalt[done++] = n;
                }
                else if (done == 1)
                {
                    while (n == generalt[0])
                    {
                        n = this.r.Next(1, 5);
                    }

                    generalt[done++] = n;
                }
                else if (done == 2)
                {
                    while (n == generalt[0] || n == generalt[1])
                    {
                        n = this.r.Next(1, 5);
                    }

                    generalt[done++] = n;
                }
                else if (done == 3)
                {
                    while (n == generalt[0] || n == generalt[1] || n == generalt[2])
                    {
                        n = this.r.Next(1, 5);
                    }

                    generalt[done++] = n;
                }
            }

            return generalt;
        }

        /// <inheritdoc/>
        public int MegoldasHandler(int lvl)
        {
            if (lvl == 1)
            {
                this.megoldasok[0] = this.Megoldas(lvl);
                return this.megoldasok[0];
            }
            else if (lvl == 2)
            {
                this.megoldasok[1] = this.Megoldas(lvl);
                return this.megoldasok[1];
            }
            else if (lvl == 3)
            {
                this.megoldasok[2] = this.Megoldas(lvl);
                return this.megoldasok[2];
            }
            else if (lvl == 4)
            {
                this.megoldasok[3] = this.Megoldas(lvl);
                return this.megoldasok[3];
            }
            else
            {
                this.megoldasok[4] = this.Megoldas(lvl);
                return this.megoldasok[4];
            }
        }

        private int Megoldas(int lvl)
        {
            if (lvl == 1)
            {
                switch (this.kijelzo)
                {
                    case 3:
                        return 3;
                    case 4:
                        return 4;
                    default:
                        return 2;
                }
            }
            else if (lvl == 2)
            {
                switch (this.kijelzo)
                {
                    case 1:
                        return this.ErtekKeres(4, this.Gombok2);
                    case 3:
                        return 1;
                    default:
                        return this.megoldasok[0];
                }
            }
            else if (lvl == 3)
            {
                switch (this.kijelzo)
                {
                    case 1:
                        return this.ErtekKeres(this.Gombok2[this.megoldasok[1] - 1], this.Gombok3);
                    case 2:
                        return this.ErtekKeres(this.Gombok1[this.megoldasok[0] - 1], this.Gombok3);
                    case 3:
                        return 3;
                    default:
                        return this.ErtekKeres(4, this.Gombok3);
                }
            }
            else if (lvl == 4)
            {
                switch (this.kijelzo)
                {
                    case 1:
                        return this.megoldasok[0];
                    case 2:
                        return 1;
                    default:
                        return this.megoldasok[1];
                }
            }
            else
            {
                switch (this.kijelzo)
                {
                    case 1:
                        return this.megoldasok[0];
                    case 2:
                        return this.megoldasok[1];
                    case 3:
                        return this.megoldasok[3];
                    default:
                        return this.megoldasok[2];
                }
            }
        }

        private int ErtekKeres(int ertek, int[] gombok)
        {
            for (int i = 0; i < gombok.Length; i++)
            {
                if (gombok[i] == ertek)
                {
                    return i + 1;
                }
            }

            return 0;
        }
    }
}
