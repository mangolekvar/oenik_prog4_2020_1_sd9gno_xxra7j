﻿// <copyright file="IBetusGombokLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// interface for lettered buttons module.
    /// </summary>
    internal interface IBetusGombokLogic
    {
        /// <summary>
        /// Generates the letters for the buttons.
        /// </summary>
        /// <param name="tarolo">that's where the result will go.</param>
        void Generalas(ref char[] tarolo);

        /// <summary>
        /// Generates which rows to go..
        /// </summary>
        /// <returns>id of rows to use.</returns>
        int[] RandomGeneralas();

        /// <summary>
        /// Converts 4 integer to a string.
        /// </summary>
        /// <param name="a">first integer.</param>
        /// <param name="b">second integer.</param>
        /// <param name="c">third integer.</param>
        /// <param name="d">fourth integer.</param>
        /// <returns>string of the 4 integer.</returns>
        string Konvertalas(int a, int b, int c, int d);

        /// <summary>
        /// Solution for lettered buttons module.
        /// </summary>
        /// <returns>string of the buttons indexes in correct order.</returns>
        string Megoldas();
    }
}
