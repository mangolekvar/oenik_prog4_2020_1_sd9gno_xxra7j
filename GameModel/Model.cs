﻿// <copyright file="Model.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// GameModel class.
    /// </summary>
    public class Model
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        public Model()
        {
            this.HibakSzama = 0;
            this.OraModul_HatralevoIdo = 300;
            this.GombModul_Szoveg = string.Empty;
            this.BetusGombModul_Betuk = new char[4];
            this.MemoriaModul_Szamok = new int[4];

            this.Hatterkep = new Rect(0, 0, 900, 700);
            this.Robbanas = new Rect(0, 0, 900, 700);

            this.OraModul_Jelzofeny_Piros = new Rect(250, 70, 20, 20);
            this.OraModul_Jelzofeny_Zold = new Rect(250, 70, 20, 20);

            this.GombModul_Szin = ' ';
            this.GombModul_Gomb = new Rect(375, 125, 105, 105);
            this.GombModul_Teglalap = new Rect(510, 140, 30, 110);
            this.GombModul_Jelzofeny_Piros = new Rect(510, 70, 20, 20);
            this.GombModul_Jelzofeny_Zold = new Rect(510, 70, 20, 20);

            this.BetusGombModul_Gombok = new Rect[4];
            this.BetusGombModul_Gombok[0] = new Rect(590, 105, 70, 70);
            this.BetusGombModul_Gombok[1] = new Rect(590, 180, 70, 70);
            this.BetusGombModul_Gombok[2] = new Rect(665, 105, 70, 70);
            this.BetusGombModul_Gombok[3] = new Rect(665, 180, 70, 70);

            this.BetusGombModul_Keret = new Rect[4];
            this.BetusGombModul_Keret[0] = new Rect(590, 105, 70, 70);
            this.BetusGombModul_Keret[1] = new Rect(590, 180, 70, 70);
            this.BetusGombModul_Keret[2] = new Rect(665, 105, 70, 70);
            this.BetusGombModul_Keret[3] = new Rect(665, 180, 70, 70);

            this.BetusGombModul_Correct = new bool[4];
            this.BetusGombModul_Jelzofeny_Piros = new Rect(735, 70, 20, 20);
            this.BetusGombModul_Jelzofeny_Zold = new Rect(735, 70, 20, 20);

            this.SimonModul_Negyzetek = new Rect[4];
            this.SimonModul_Szinek = "RYBG";
            this.SimonModul_Vilagit = new bool[4];
            this.SimonModul_Negyzetek[0] = new Rect(145, 365, 55, 55);
            this.SimonModul_Negyzetek[1] = new Rect(205, 365, 55, 55);
            this.SimonModul_Negyzetek[2] = new Rect(145, 425, 55, 55);
            this.SimonModul_Negyzetek[3] = new Rect(205, 425, 55, 55);
            this.SimonLenyomott = string.Empty;
            this.SimonModul_IsLevelDone = new bool[3];
            this.SimonModul_LevelTeglalap = new Rect[3];
            this.SimonModul_LevelTeglalap[0] = new Rect(275, 420, 25, 15);
            this.SimonModul_LevelTeglalap[1] = new Rect(275, 395, 25, 15);
            this.SimonModul_LevelTeglalap[2] = new Rect(275, 370, 25, 15);

            this.SimonModul_Jelzofeny_Piros = new Rect(265, 330, 20, 20);
            this.SimonModul_Jelzofeny_Zold = new Rect(265, 330, 20, 20);

            this.Memoria_IsLevelDone = new bool[5];
            this.MemoriaModul_SzamosTeglalapok = new Rect[4];
            this.MemoriaModul_SzamosTeglalapok[0] = new Rect(350, 445, 30, 50);
            this.MemoriaModul_SzamosTeglalapok[1] = new Rect(385, 445, 30, 50);
            this.MemoriaModul_SzamosTeglalapok[2] = new Rect(420, 445, 30, 50);
            this.MemoriaModul_SzamosTeglalapok[3] = new Rect(455, 445, 30, 50);

            this.MemoriaModul_KisTeglalapok = new Rect[5];
            this.MemoriaModul_KisTeglalapok[4] = new Rect(501, 380, 28, 15);
            this.MemoriaModul_KisTeglalapok[3] = new Rect(501, 405, 28, 15);
            this.MemoriaModul_KisTeglalapok[2] = new Rect(501, 430, 28, 15);
            this.MemoriaModul_KisTeglalapok[1] = new Rect(501, 455, 28, 15);
            this.MemoriaModul_KisTeglalapok[0] = new Rect(501, 480, 28, 15);
            this.MemoriaModul_KisTeglalapok_Zold = new Rect[5];
            this.MemoriaModul_KisTeglalapok_Zold[4] = new Rect(501, 380, 28, 15);
            this.MemoriaModul_KisTeglalapok_Zold[3] = new Rect(501, 405, 28, 15);
            this.MemoriaModul_KisTeglalapok_Zold[2] = new Rect(501, 430, 28, 15);
            this.MemoriaModul_KisTeglalapok_Zold[1] = new Rect(501, 455, 28, 15);
            this.MemoriaModul_KisTeglalapok_Zold[0] = new Rect(501, 480, 28, 15);

            this.MemoriaModul_Jelzofeny_Piros = new Rect(505, 330, 20, 20);
            this.MemoriaModul_Jelzofeny_Zold = new Rect(505, 330, 20, 20);

            this.VezetekekModul_Vezetekek = new Rect[6];
            this.VezetekekSet = new char[6];
            this.VezetekekModul_Vezetekek[0] = new Rect(600, 370, 145, 10);
            this.VezetekekModul_Vezetekek[1] = new Rect(613, 400, 150, 10);
            this.VezetekekModul_Vezetekek[2] = new Rect(594, 423, 170, 10);
            this.VezetekekModul_Vezetekek[3] = new Rect(607, 445, 155, 10);
            this.VezetekekModul_Vezetekek[4] = new Rect(603, 470, 145, 10);
            this.VezetekekModul_Vezetekek[5] = new Rect(610, 495, 150, 10);

            this.VezetekekModul_Vezetekek_Elvagott = new Rect[6];
            this.VezetekekModul_Vezetekek_Elvagott[0] = new Rect(600, 370, 145, 10);
            this.VezetekekModul_Vezetekek_Elvagott[1] = new Rect(613, 400, 150, 10);
            this.VezetekekModul_Vezetekek_Elvagott[2] = new Rect(594, 423, 170, 10);
            this.VezetekekModul_Vezetekek_Elvagott[3] = new Rect(607, 445, 155, 10);
            this.VezetekekModul_Vezetekek_Elvagott[4] = new Rect(603, 470, 145, 10);
            this.VezetekekModul_Vezetekek_Elvagott[5] = new Rect(610, 495, 150, 10);

            this.IsElvagott = new bool[6];

            this.VezetekekModul_Jelzofeny_Piros = new Rect(740, 330, 20, 20);
            this.VezetekekModul_Jelzofeny_Zold = new Rect(740, 330, 20, 20);
        }

        /// <summary>
        /// Gets or sets Bomb Rectangle.
        /// </summary>
        public Rect Hatterkep { get; set; }

        /// <summary>
        /// Gets or sets Explosion Rectangle.
        /// </summary>
        public Rect Robbanas { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: error.
        /// </summary>
        public bool Hiba { get; set; }

        /// <summary>
        /// Gets or sets number of errors.
        /// </summary>
        public int HibakSzama { get; set; }

        /// <summary>
        /// Gets or sets remaining time.
        /// </summary>
        public int OraModul_HatralevoIdo { get; set; }

        /// <summary>
        /// Gets or sets Clock module red light.
        /// </summary>
        public Rect OraModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets Clock module green light.
        /// </summary>
        public Rect OraModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: clock module is done.
        /// </summary>
        public bool OraModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets button module text.
        /// </summary>
        public string GombModul_Szoveg { get; set; }

        /// <summary>
        /// Gets or sets button module button.
        /// </summary>
        public Rect GombModul_Gomb { get; set; }

        /// <summary>
        /// Gets or sets button module rectangle.
        /// </summary>
        public Rect GombModul_Teglalap { get; set; }

        /// <summary>
        /// Gets or sets button module red light.
        /// </summary>
        public Rect GombModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets button module green light.
        /// </summary>
        public Rect GombModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: button module is done.
        /// </summary>
        public bool GombModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets button module color.
        /// </summary>
        public char GombModul_Szin { get; set; }

        /// <summary>
        /// Gets or sets letter module buttons.
        /// </summary>
        public Rect[] BetusGombModul_Gombok { get; set; }

        /// <summary>
        /// Gets or sets letter module letters.
        /// </summary>
        public char[] BetusGombModul_Betuk { get; set; }

        /// <summary>
        /// Gets or sets letter module red light.
        /// </summary>
        public Rect BetusGombModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets letter module green light.
        /// </summary>
        public Rect BetusGombModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Letters module is correct.
        /// </summary>
        public bool[] BetusGombModul_Correct { get; set; }

        /// <summary>
        /// Gets or sets letter module frame.
        /// </summary>
        public Rect[] BetusGombModul_Keret { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Letters module is done.
        /// </summary>
        public bool BetusGombModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets simon module color.
        /// </summary>
        public string SimonModul_Szinek { get; set; }

        /// <summary>
        /// Gets or sets letter module squares.
        /// </summary>
        public Rect[] SimonModul_Negyzetek { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Simon module is light.
        /// </summary>
        public bool[] SimonModul_Vilagit { get; set; }

        /// <summary>
        /// Gets or sets simon module red light.
        /// </summary>
        public Rect SimonModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets simon module green light.
        /// </summary>
        public Rect SimonModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Simon module is done.
        /// </summary>
        public bool SimonModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets simon module pressed.
        /// </summary>
        public string SimonLenyomott { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Simon module is level done.
        /// </summary>
        public bool[] SimonModul_IsLevelDone { get; set; }

        /// <summary>
        /// Gets or sets simon module level rect.
        /// </summary>
        public Rect[] SimonModul_LevelTeglalap { get; set; }

        /// <summary>
        /// Gets or sets memory module number rects.
        /// </summary>
        public Rect[] MemoriaModul_SzamosTeglalapok { get; set; }

        /// <summary>
        /// Gets or sets memory module numbers.
        /// </summary>
        public int[] MemoriaModul_Szamok { get; set; }

        /// <summary>
        /// Gets or sets simon module upper number.
        /// </summary>
        public int MemoriaModul_FelsoSzam { get; set; }

        /// <summary>
        /// Gets or sets simon module rects.
        /// </summary>
        public Rect[] MemoriaModul_KisTeglalapok { get; set; }

        /// <summary>
        /// Gets or sets simon module rects green.
        /// </summary>
        public Rect[] MemoriaModul_KisTeglalapok_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Memory module is level done.
        /// </summary>
        public bool[] Memoria_IsLevelDone { get; set; }

        /// <summary>
        /// Gets or sets simon module red light.
        /// </summary>
        public Rect MemoriaModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets simon module green light.
        /// </summary>
        public Rect MemoriaModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Memory module is done.
        /// </summary>
        public bool MemoriaModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets wire module wires.
        /// </summary>
        public Rect[] VezetekekModul_Vezetekek { get; set; }

        /// <summary>
        /// Gets or sets wire module wires set.
        /// </summary>
        public char[] VezetekekSet { get; set; }

        /// <summary>
        /// Gets or sets wire module red light.
        /// </summary>
        public Rect VezetekekModul_Jelzofeny_Piros { get; set; }

        /// <summary>
        /// Gets or sets wire module green light.
        /// </summary>
        public Rect VezetekekModul_Jelzofeny_Zold { get; set; }

        /// <summary>
        /// Gets or sets wire module wires cutted.
        /// </summary>
        public Rect[] VezetekekModul_Vezetekek_Elvagott { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Is wire cut.
        /// </summary>
        public bool[] IsElvagott { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: Wire module is done.
        /// </summary>
        public bool VezetekekModul_IsDone { get; set; }

        /// <summary>
        /// Gets or sets serial number.
        /// </summary>
        public string SerialNumber { get; set; }
    }
}
