﻿// <copyright file="Control.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using GameLogic;
    using GameModel;
    using GameRenderer;

    /// <summary>
    /// Game controller.
    /// </summary>
    public class Control : FrameworkElement
    {
        private Model model;
        private Renderer renderer;
        private GameLogic logic;
        private DispatcherTimer tickTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Game Control load.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new Model();
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);

            Window win = Window.GetWindow(this);

            for (int i = 0; i < this.logic.Vezetekek.Length; i++)
            {
                this.model.VezetekekSet[i] = this.logic.Vezetekek[i];
            }

            for (int i = 0; i < this.logic.Betuk.Length; i++)
            {
                this.model.BetusGombModul_Betuk[i] = this.logic.Betuk[i];
            }

            int s = 0;
            while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[0])
            {
                s++;
            }

            this.model.SimonModul_Vilagit[s] = true;

            this.model.SerialNumber = this.logic.SerialNumber();

            this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;

            this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;

            this.model.GombModul_Szin = this.logic.AGombSzin;

            this.model.GombModul_Szoveg = this.logic.AGombSzoveg;

            this.model.HibakSzama = this.logic.Hibak;

            if (win != null)
            {
                this.tickTimer = new DispatcherTimer();
                this.tickTimer.Interval = TimeSpan.FromSeconds(1);
                this.tickTimer.Tick += this.Idozito;
                this.tickTimer.Start();

                this.MouseRightButtonDown += this.GombModul_MouseRightButtonDown;
                this.MouseLeftButtonDown += this.GombModul_MouseLeftButtonDown;

                this.MouseLeftButtonDown += this.SimonModul_JobbAlso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.SimonModul_BalAlso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.SimonModul_JobbFelso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.SimonModul_BalFelso_MouseLeftButtonDown;

                this.MouseLeftButtonDown += this.VezetekekModul_0_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.VezetekekModul_1_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.VezetekekModul_2_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.VezetekekModul_3_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.VezetekekModul_4_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.VezetekekModul_5_MouseLeftButtonDown;

                this.MouseLeftButtonDown += this.MemoriaModul_Elso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.MemoriaModul_Masodik_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.MemoriaModul_Harmadik_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.MemoriaModul_Negyedik_MouseLeftButtonDown;

                this.MouseLeftButtonDown += this.BetusGombok_BalFelso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.BetusGombok_JobbFelso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.BetusGombok_BalAlso_MouseLeftButtonDown;
                this.MouseLeftButtonDown += this.BetusGombok_JobbAlso_MouseLeftButtonDown;
            }

            this.logic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
        }

        /// <summary>
        /// Letters module button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void BetusGombok_BalFelso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 590 && e.GetPosition(this).X < 660 && e.GetPosition(this).Y > 105 && e.GetPosition(this).Y < 175)
            {
                int j = 0;
                for (int i = 0; i < this.model.BetusGombModul_Correct.Length; i++)
                {
                    if (this.model.BetusGombModul_Correct[i] == true)
                    {
                        j++;
                    }
                }

                if (this.logic.BetusGombokMegoldas.ToString()[0] == '1' && j == 0)
                {
                    this.model.BetusGombModul_Correct[0] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[1] == '1' && j == 1)
                {
                    this.model.BetusGombModul_Correct[0] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[2] == '1' && j == 2)
                {
                    this.model.BetusGombModul_Correct[0] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[3] == '1' && j == 3)
                {
                    this.model.BetusGombModul_Correct[0] = true;
                    j++;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (j == 4)
                {
                    this.model.BetusGombModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Letters module button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void BetusGombok_BalAlso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 590 && e.GetPosition(this).X < 660 && e.GetPosition(this).Y > 180 && e.GetPosition(this).Y < 250)
            {
                int j = 0;
                for (int i = 0; i < this.model.BetusGombModul_Correct.Length; i++)
                {
                    if (this.model.BetusGombModul_Correct[i] == true)
                    {
                        j++;
                    }
                }

                if (this.logic.BetusGombokMegoldas.ToString()[0] == '2' && j == 0)
                {
                    this.model.BetusGombModul_Correct[1] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[1] == '2' && j == 1)
                {
                    this.model.BetusGombModul_Correct[1] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[2] == '2' && j == 2)
                {
                    this.model.BetusGombModul_Correct[1] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[3] == '2' && j == 3)
                {
                    this.model.BetusGombModul_Correct[1] = true;
                    j++;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (j == 4)
                {
                    this.model.BetusGombModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Letters module button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void BetusGombok_JobbFelso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 665 && e.GetPosition(this).X < 735 && e.GetPosition(this).Y > 105 && e.GetPosition(this).Y < 175)
            {
                int j = 0;
                for (int i = 0; i < this.model.BetusGombModul_Correct.Length; i++)
                {
                    if (this.model.BetusGombModul_Correct[i] == true)
                    {
                        j++;
                    }
                }

                if (this.logic.BetusGombokMegoldas.ToString()[0] == '3' && j == 0)
                {
                    this.model.BetusGombModul_Correct[2] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[1] == '3' && j == 1)
                {
                    this.model.BetusGombModul_Correct[2] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[2] == '3' && j == 2)
                {
                    this.model.BetusGombModul_Correct[2] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[3] == '3' && j == 3)
                {
                    this.model.BetusGombModul_Correct[2] = true;
                    j++;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (j == 4)
                {
                    this.model.BetusGombModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Letters module button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void BetusGombok_JobbAlso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 665 && e.GetPosition(this).X < 735 && e.GetPosition(this).Y > 180 && e.GetPosition(this).Y < 250)
            {
                int j = 0;
                for (int i = 0; i < this.model.BetusGombModul_Correct.Length; i++)
                {
                    if (this.model.BetusGombModul_Correct[i] == true)
                    {
                        j++;
                    }
                }

                if (this.logic.BetusGombokMegoldas.ToString()[0] == '4' && j == 0)
                {
                    this.model.BetusGombModul_Correct[3] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[1] == '4' && j == 1)
                {
                    this.model.BetusGombModul_Correct[3] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[2] == '4' && j == 2)
                {
                    this.model.BetusGombModul_Correct[3] = true;
                    j++;
                }
                else if (this.logic.BetusGombokMegoldas.ToString()[3] == '4' && j == 3)
                {
                    this.model.BetusGombModul_Correct[3] = true;
                    j++;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (j == 4)
                {
                    this.model.BetusGombModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Memory module 1. button.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void MemoriaModul_Elso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 350 && e.GetPosition(this).X < 380 && e.GetPosition(this).Y > 445 && e.GetPosition(this).Y < 495)
            {
                if (this.logic.MemoriaEllenorzes(1))
                {
                    int i = 0;
                    while (this.model.Memoria_IsLevelDone[i] == true)
                    {
                        i++;
                    }

                    this.model.Memoria_IsLevelDone[i] = true;
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                }
                else
                {
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                    this.model.HibakSzama = this.logic.Hibak;
                    for (int i = 0; i < this.model.Memoria_IsLevelDone.Length; i++)
                    {
                        this.model.Memoria_IsLevelDone[i] = false;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.logic.MemoriaKesz())
                {
                    this.model.MemoriaModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Memory module 2. button.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void MemoriaModul_Masodik_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 385 && e.GetPosition(this).X < 415 && e.GetPosition(this).Y > 445 && e.GetPosition(this).Y < 495)
            {
                if (this.logic.MemoriaEllenorzes(2))
                {
                    int i = 0;
                    while (this.model.Memoria_IsLevelDone[i] == true)
                    {
                        i++;
                    }

                    this.model.Memoria_IsLevelDone[i] = true;
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                }
                else
                {
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                    this.model.HibakSzama = this.logic.Hibak;
                    for (int i = 0; i < this.model.Memoria_IsLevelDone.Length; i++)
                    {
                        this.model.Memoria_IsLevelDone[i] = false;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.logic.MemoriaKesz())
                {
                    this.model.MemoriaModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Memory module 3. button.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void MemoriaModul_Harmadik_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 420 && e.GetPosition(this).X < 455 && e.GetPosition(this).Y > 445 && e.GetPosition(this).Y < 495)
            {
                if (this.logic.MemoriaEllenorzes(3))
                {
                    int i = 0;
                    while (this.model.Memoria_IsLevelDone[i] == true)
                    {
                        i++;
                    }

                    this.model.Memoria_IsLevelDone[i] = true;
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                }
                else
                {
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                    this.model.HibakSzama = this.logic.Hibak;
                    for (int i = 0; i < this.model.Memoria_IsLevelDone.Length; i++)
                    {
                        this.model.Memoria_IsLevelDone[i] = false;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.logic.MemoriaKesz())
                {
                    this.model.MemoriaModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Memory module 4. button.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Paramerter.</param>
        private void MemoriaModul_Negyedik_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 455 && e.GetPosition(this).X < 485 && e.GetPosition(this).Y > 445 && e.GetPosition(this).Y < 495)
            {
                if (this.logic.MemoriaEllenorzes(4))
                {
                    int i = 0;
                    while (this.model.Memoria_IsLevelDone[i] == true)
                    {
                        i++;
                    }

                    this.model.Memoria_IsLevelDone[i] = true;
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                }
                else
                {
                    this.model.MemoriaModul_FelsoSzam = this.logic.Kijelzo;
                    this.model.MemoriaModul_Szamok = this.logic.MemoriaGombok;
                    this.model.HibakSzama = this.logic.Hibak;
                    for (int i = 0; i < this.model.Memoria_IsLevelDone.Length; i++)
                    {
                        this.model.Memoria_IsLevelDone[i] = false;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.logic.MemoriaKesz())
                {
                    this.model.MemoriaModul_IsDone = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 5. wire.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 610 && e.GetPosition(this).X < 760 && e.GetPosition(this).Y > 495 && e.GetPosition(this).Y < 505)
            {
                if (this.logic.VezetekekMegoldas == 5)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[5] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 4. wire event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 603 && e.GetPosition(this).X < 748 && e.GetPosition(this).Y > 470 && e.GetPosition(this).Y < 480)
            {
                if (this.logic.VezetekekMegoldas == 4)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[4] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 3. wire event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 607 && e.GetPosition(this).X < 758 && e.GetPosition(this).Y > 445 && e.GetPosition(this).Y < 455)
            {
                if (this.logic.VezetekekMegoldas == 3)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[3] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 2. wire event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 594 && e.GetPosition(this).X < 742 && e.GetPosition(this).Y > 423 && e.GetPosition(this).Y < 433)
            {
                if (this.logic.VezetekekMegoldas == 2)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[2] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 1. wire event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 613 && e.GetPosition(this).X < 763 && e.GetPosition(this).Y > 400 && e.GetPosition(this).Y < 410)
            {
                if (this.logic.VezetekekMegoldas == 1)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[1] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Wire module 0. wire event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void VezetekekModul_0_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 600 && e.GetPosition(this).X < 745 && e.GetPosition(this).Y > 370 && e.GetPosition(this).Y < 380)
            {
                if (this.logic.VezetekekMegoldas == 0)
                {
                    this.model.VezetekekModul_IsDone = true;
                    this.model.IsElvagott[0] = true;
                }
                else
                {
                    this.logic.HibaNovelo();
                    this.model.HibakSzama = this.logic.Hibak;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Simon module red button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void SimonModul_BalFelso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 145 && e.GetPosition(this).X < 200 && e.GetPosition(this).Y > 365 && e.GetPosition(this).Y < 420)
            {
                this.model.SimonLenyomott += "R";

                this.Simon();

                if (this.logic.SimonMondjaKesz())
                {
                    this.model.SimonModul_IsDone = true;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Simon module yellow button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void SimonModul_JobbFelso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 205 && e.GetPosition(this).X < 260 && e.GetPosition(this).Y > 365 && e.GetPosition(this).Y < 420)
            {
                this.model.SimonLenyomott += "Y";

                this.Simon();

                if (this.logic.SimonMondjaKesz())
                {
                    this.model.SimonModul_IsDone = true;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Simon module blue button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void SimonModul_BalAlso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 145 && e.GetPosition(this).X < 200 && e.GetPosition(this).Y > 425 && e.GetPosition(this).Y < 480)
            {
                this.model.SimonLenyomott += "B";

                this.Simon();

                if (this.logic.SimonMondjaKesz())
                {
                    this.model.SimonModul_IsDone = true;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Simon module green button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void SimonModul_JobbAlso_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 205 && e.GetPosition(this).X < 260 && e.GetPosition(this).Y > 425 && e.GetPosition(this).Y < 480)
            {
                this.model.SimonLenyomott += "G";

                this.Simon();

                if (this.logic.SimonMondjaKesz())
                {
                    this.model.SimonModul_IsDone = true;
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Simon module method.
        /// </summary>
        private void Simon()
        {
            if (this.logic.SMLevel == 1)
            {
                if (this.model.SimonLenyomott.Length == 1)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[1])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 2)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[2])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[0])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
            }
            else if (this.logic.SMLevel == 2)
            {
                if (this.model.SimonLenyomott.Length == 0)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[0])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 1)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[1])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 2)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[2])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 3)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[3])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[0])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
            }
            else if (this.logic.SMLevel == 3)
            {
                if (this.model.SimonLenyomott.Length == 0)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[0])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 1)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[1])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 2)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[2])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 3)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[3])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
                else if (this.model.SimonLenyomott.Length == 4)
                {
                    int s = 0;
                    while (this.model.SimonModul_Szinek[s] != this.logic.SimonMondjaSzinek[4])
                    {
                        s++;
                    }

                    for (int i = 0; i < this.model.SimonModul_Vilagit.Length; i++)
                    {
                        this.model.SimonModul_Vilagit[i] = false;
                    }

                    this.model.SimonModul_Vilagit[s] = true;
                }
            }

            if (this.logic.SMLevel == 1)
            {
                if (this.model.SimonLenyomott.Length == 3)
                {
                    if (this.logic.SimonMondjaEllenorzes(this.model.SimonLenyomott))
                    {
                        this.model.SimonModul_IsLevelDone[0] = true;
                        this.model.SimonLenyomott = string.Empty;
                    }
                    else
                    {
                        this.model.HibakSzama = this.logic.Hibak;
                        this.model.SimonLenyomott = string.Empty;
                    }
                }
            }
            else if (this.logic.SMLevel == 2)
            {
                if (this.model.SimonLenyomott.Length == 4)
                {
                    if (this.logic.SimonMondjaEllenorzes(this.model.SimonLenyomott))
                    {
                        this.model.SimonModul_IsLevelDone[1] = true;
                        this.model.SimonLenyomott = string.Empty;
                    }
                    else
                    {
                        this.model.HibakSzama = this.logic.Hibak;
                        this.model.SimonLenyomott = string.Empty;
                    }
                }
            }
            else if (this.logic.SMLevel == 3)
            {
                if (this.model.SimonLenyomott.Length == 5)
                {
                    if (this.logic.SimonMondjaEllenorzes(this.model.SimonLenyomott))
                    {
                        this.model.SimonModul_IsLevelDone[2] = true;
                        this.model.SimonLenyomott = string.Empty;
                    }
                    else
                    {
                        this.model.HibakSzama = this.logic.Hibak;
                        this.model.SimonLenyomott = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Button module left button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void GombModul_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 365 && e.GetPosition(this).X < 485 && e.GetPosition(this).Y > 125 && e.GetPosition(this).Y < 230)
            {
                if (this.logic.AGombMegoldas == 2 || this.logic.AGombMegoldas == 1)
                {
                    if (this.model.GombModul_Szoveg == "Detonate")
                    {
                        this.model.GombModul_IsDone = true;
                    }
                    else if (this.logic.AGombEllenorzes())
                    {
                        this.model.GombModul_IsDone = true;
                    }
                    else
                    {
                        this.model.HibakSzama = this.logic.Hibak;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Button module right button event.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void GombModul_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(this).X > 365 && e.GetPosition(this).X < 485 && e.GetPosition(this).Y > 125 && e.GetPosition(this).Y < 230)
            {
                if (this.logic.AGombMegoldas == 3)
                {
                    if (this.logic.AGombEllenorzes())
                    {
                        this.model.GombModul_IsDone = true;
                    }
                    else
                    {
                        this.model.HibakSzama = this.logic.Hibak;
                    }
                }

                if (this.logic.Hibak == 3)
                {
                    this.model.Hiba = true;
                }

                if (this.model.BetusGombModul_IsDone && this.model.GombModul_IsDone && this.model.MemoriaModul_IsDone && this.model.SimonModul_IsDone && this.model.VezetekekModul_IsDone)
                {
                    this.model.OraModul_IsDone = true;
                }
            }
        }

        /// <summary>
        /// Timer method.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Parameter.</param>
        private void Idozito(object sender, EventArgs e)
        {
            this.logic.IdozitoCsokkenes();
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.DrawThings(drawingContext);
            }
        }
    }
}
