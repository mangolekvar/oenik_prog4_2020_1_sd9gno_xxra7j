﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;
    using NUnit.Framework;

    /// <summary>
    /// Tests for GameLogic.
    /// </summary>
    public class LogicTests
    {
        /// <summary>
        /// Test of wires module, where there's 6 wire and the second condition should work.
        /// </summary>
        [Test]
        public void Test_Vezetek_6_2()
        {
            Model test1 = new Model();
            GameLogic logic1 = new GameLogic(test1);
            logic1.Vl.Vezetekek = new char[] { 'F', 'W', 'K', 'K', 'S', 'P' };
            logic1.Vl.Fekete = 1;
            logic1.Vl.Kek = 2;

            int expected = 5;

            int actual = logic1.Vl.Megoldas();

            Assert.IsFalse(expected == actual);
        }

        /// <summary>
        /// Test of The Button module.
        /// </summary>
        [Test]
        public void Test_AGomb()
        {
            Model test2 = new Model();
            GameLogic logic2 = new GameLogic(test2);
            logic2.Agl.Szin = 'Y';
            logic2.Agl.Szoveg = "Abort";

            int timer = 294;

            string actual = logic2.Agl.GombFelengedes();

            Assert.IsTrue(timer.ToString().Contains(actual));
        }

        /// <summary>
        /// Test of lettered buttons module.
        /// </summary>
        [Test]
        public void Test_BetusGombok()
        {
            Model test3 = new Model();
            GameLogic logic3 = new GameLogic(test3);
            logic3.Bgl.Gombok = new char[] { 'Ä', '§', '>', '=' };
            logic3.Bgl.Sorok = new int[] { 3, 4, 5, 6 };

            string expected = "1234";

            string actual = logic3.Bgl.Megoldas();

            Assert.IsTrue(expected == actual);
        }

        /// <summary>
        /// test of simon says module.
        /// </summary>
        [Test]
        public void Test_SimonMondja()
        {
            Model test4 = new Model();
            GameLogic logic4 = new GameLogic(test4);
            logic4.Sml.Inditas(true);
            char expected = ' ';

            if (logic4.Sml.Szinek[0] == 'Y')
            {
                expected = 'R';
            }
            else if (logic4.Sml.Szinek[0] == 'R')
            {
                expected = 'Y';
            }
            else if (logic4.Sml.Szinek[0] == 'B')
            {
                expected = 'G';
            }
            else
            {
                expected = 'B';
            }

            Assert.That(logic4.Sml.Megoldas0[0] == expected);
        }

        /// <summary>
        /// test of memory module.
        /// </summary>
        [Test]
        public void Test_Memoria()
        {
            Model test5 = new Model();
            GameLogic logic5 = new GameLogic(test5);
            int expected = 0;

            if (logic5.Ml.Kijelzo == 1 || logic5.Ml.Kijelzo == 2)
            {
                expected = 2;
            }
            else if (logic5.Ml.Kijelzo == 3)
            {
                expected = 3;
            }
            else
            {
                expected = 4;
            }

            Assert.IsTrue(logic5.MemoriaEllenorzes(expected));
        }
    }
}
