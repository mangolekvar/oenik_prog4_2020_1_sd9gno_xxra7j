﻿// <copyright file="Renderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using GameModel;

    /// <summary>
    /// Game renderer class.
    /// </summary>
    public class Renderer
    {
        private Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="model">gamemodel.</param>
        public Renderer(Model model)
        {
            this.model = model;
        }

        /// <summary>
        /// Drawing things method.
        /// </summary>
        /// <param name="context">Context.</param>
        public void DrawThings(DrawingContext context)
        {
            DrawingGroup dg = new DrawingGroup();
            string workingDirectory = Environment.CurrentDirectory;
            ImageDrawing hatter = new ImageDrawing();
            hatter.Rect = this.model.Hatterkep;
            hatter.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/bomba_alap.png", UriKind.Relative));

            dg.Children.Add(hatter);

            ImageDrawing robbanas = new ImageDrawing();
            robbanas.Rect = this.model.Robbanas;
            robbanas.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/explosion.jpg", UriKind.Relative));

            GeometryDrawing oraJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.OraModul_Jelzofeny_Piros));

            GeometryDrawing betusGombJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.BetusGombModul_Jelzofeny_Piros));

            GeometryDrawing gombJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.GombModul_Jelzofeny_Piros));

            GeometryDrawing simonJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.SimonModul_Jelzofeny_Piros));

            GeometryDrawing memoriaJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.MemoriaModul_Jelzofeny_Piros));

            GeometryDrawing vezetekJelzo = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.VezetekekModul_Jelzofeny_Piros));

            FormattedText formattedText_ido = new FormattedText(this.model.OraModul_HatralevoIdo.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.RightToLeft,
               new Typeface("Arial"),
               72,
               Brushes.Red);
            GeometryDrawing hatralevoIdo = new GeometryDrawing(null, new Pen(Brushes.Red, 6), formattedText_ido.BuildGeometry(new Point(270, 150)));

            if (this.model.HibakSzama == 0)
            {
                FormattedText formattedText_X = new FormattedText(string.Empty,
               System.Globalization.CultureInfo.CurrentCulture,
               FlowDirection.LeftToRight,
               new Typeface("Arial"),
               40,
               Brushes.Red);
                GeometryDrawing ora_X = new GeometryDrawing(null, new Pen(Brushes.Red, 4), formattedText_X.BuildGeometry(new Point(140, 75)));
                dg.Children.Add(ora_X);
            }
            else if (this.model.HibakSzama == 1)
            {
                FormattedText formattedText_X = new FormattedText("X",
               System.Globalization.CultureInfo.CurrentCulture,
               FlowDirection.LeftToRight,
               new Typeface("Arial"),
               40,
               Brushes.Red);
                GeometryDrawing ora_X = new GeometryDrawing(null, new Pen(Brushes.Red, 4), formattedText_X.BuildGeometry(new Point(140, 75)));
                dg.Children.Add(ora_X);
            }
            else
            {
                FormattedText formattedText_X = new FormattedText("XX",
               System.Globalization.CultureInfo.CurrentCulture,
               FlowDirection.LeftToRight,
               new Typeface("Arial"),
               40,
               Brushes.Red);
                GeometryDrawing ora_X = new GeometryDrawing(null, new Pen(Brushes.Red, 4), formattedText_X.BuildGeometry(new Point(140, 75)));
                dg.Children.Add(ora_X);
            }

            FormattedText formattedText_BetusGombBetu0 = new FormattedText(this.model.BetusGombModul_Betuk[0].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing betusGomb_betu0 = new GeometryDrawing(null, new Pen(Brushes.Blue, 1), formattedText_BetusGombBetu0.BuildGeometry(new System.Windows.Point(615, 120)));

            FormattedText formattedText_BetusGombBetu1 = new FormattedText(this.model.BetusGombModul_Betuk[1].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing betusGomb_betu1 = new GeometryDrawing(null, new Pen(Brushes.Blue, 1), formattedText_BetusGombBetu1.BuildGeometry(new Point(615, 195)));

            FormattedText formattedText_BetusGombBetu2 = new FormattedText(this.model.BetusGombModul_Betuk[2].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing betusGomb_betu2 = new GeometryDrawing(null, new Pen(Brushes.Blue, 1), formattedText_BetusGombBetu2.BuildGeometry(new Point(690, 120)));

            FormattedText formattedText_BetusGombBetu3 = new FormattedText(this.model.BetusGombModul_Betuk[3].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing betusGomb_betu3 = new GeometryDrawing(null, new Pen(Brushes.Blue, 1), formattedText_BetusGombBetu3.BuildGeometry(new Point(690, 195)));

            FormattedText formattedText_MemoriaSzam0 = new FormattedText(this.model.MemoriaModul_Szamok[0].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing memoria_szam0 = new GeometryDrawing(null, new Pen(Brushes.Black, 1), formattedText_MemoriaSzam0.BuildGeometry(new Point(355, 450)));

            FormattedText formattedText_MemoriaSzam1 = new FormattedText(this.model.MemoriaModul_Szamok[1].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing memoria_szam1 = new GeometryDrawing(null, new Pen(Brushes.Black, 1), formattedText_MemoriaSzam1.BuildGeometry(new Point(390, 450)));

            FormattedText formattedText_MemoriaSzam2 = new FormattedText(this.model.MemoriaModul_Szamok[2].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing memoria_szam2 = new GeometryDrawing(null, new Pen(Brushes.Black, 1), formattedText_MemoriaSzam2.BuildGeometry(new Point(425, 450)));

            FormattedText formattedText_MemoriaSzam3 = new FormattedText(this.model.MemoriaModul_Szamok[3].ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            35,
            Brushes.White);
            GeometryDrawing memoria_szam3 = new GeometryDrawing(null, new Pen(Brushes.Black, 1), formattedText_MemoriaSzam3.BuildGeometry(new Point(460, 450)));

            FormattedText formattedText_MemoriaFelsoSzam = new FormattedText(this.model.MemoriaModul_FelsoSzam.ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            50,
            Brushes.White);
            GeometryDrawing memoria_felsoSzam = new GeometryDrawing(null, new Pen(Brushes.White, 3), formattedText_MemoriaFelsoSzam.BuildGeometry(new Point(405, 370)));

            FormattedText formattedText_SerialNumber = new FormattedText(this.model.SerialNumber.ToString(),
            System.Globalization.CultureInfo.CurrentCulture,
            FlowDirection.LeftToRight,
            new Typeface("Arial"),
            25,
            Brushes.White);
            GeometryDrawing serialNumber = new GeometryDrawing(null, new Pen(Brushes.DarkRed, 2), formattedText_SerialNumber.BuildGeometry(new Point(605, 570)));

            if (this.model.GombModul_Szin == 'B')
            {
                ImageDrawing gomb_kek = new ImageDrawing();
                gomb_kek.Rect = this.model.GombModul_Gomb;
                gomb_kek.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/blue_button.png", UriKind.Relative));
                dg.Children.Add(gomb_kek);
            }
            else if (this.model.GombModul_Szin == 'F')
            {
                ImageDrawing gomb_fekete = new ImageDrawing();
                gomb_fekete.Rect = this.model.GombModul_Gomb;
                gomb_fekete.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/black_button.png", UriKind.Relative));
                dg.Children.Add(gomb_fekete);
            }
            else if (this.model.GombModul_Szin == 'W')
            {
                ImageDrawing gomb_feher = new ImageDrawing();
                gomb_feher.Rect = this.model.GombModul_Gomb;
                gomb_feher.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/white_button.png", UriKind.Relative));
                dg.Children.Add(gomb_feher);
            }
            else if (this.model.GombModul_Szin == 'Y')
            {
                ImageDrawing gomb_sarga = new ImageDrawing();
                gomb_sarga.Rect = this.model.GombModul_Gomb;
                gomb_sarga.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/yellow_button.png", UriKind.Relative));

                dg.Children.Add(gomb_sarga);
            }
            else if (this.model.GombModul_Szin == 'R')
            {
                ImageDrawing gomb_piros = new ImageDrawing();
                gomb_piros.Rect = this.model.GombModul_Gomb;
                gomb_piros.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/red_button.png", UriKind.Relative));
                dg.Children.Add(gomb_piros);
            }
            else if (this.model.GombModul_Szin == 'G')
            {
                ImageDrawing gomb_piros = new ImageDrawing();
                gomb_piros.Rect = this.model.GombModul_Gomb;
                gomb_piros.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/green_button.png", UriKind.Relative));
                dg.Children.Add(gomb_piros);
            }

            if (this.model.GombModul_Szoveg == "Detonate")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText(this.model.GombModul_Szoveg.ToString(),
              System.Globalization.CultureInfo.CurrentCulture,
              System.Windows.FlowDirection.LeftToRight,
              new Typeface("Arial"),
              20,
              Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }
            else if (this.model.GombModul_Szoveg == "Abort")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText("  " + this.model.GombModul_Szoveg.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.LeftToRight,
               new Typeface("Arial"),
               20,
               Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }
            else if (this.model.GombModul_Szoveg == "Car")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText("  " + this.model.GombModul_Szoveg.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.LeftToRight,
               new Typeface("Arial"),
               20,
               Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }
            else if (this.model.GombModul_Szoveg == "Batteries")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText(this.model.GombModul_Szoveg.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.LeftToRight,
               new Typeface("Arial"),
               20,
               Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }
            else if (this.model.GombModul_Szoveg == "Hold")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText("   " + this.model.GombModul_Szoveg.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.LeftToRight,
               new Typeface("Arial"),
               20,
               Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }
            else if (this.model.GombModul_Szoveg == "Press")
            {
                FormattedText formattedText_Gomb_Szoveg = new FormattedText("  " + this.model.GombModul_Szoveg.ToString(),
               System.Globalization.CultureInfo.CurrentCulture,
               System.Windows.FlowDirection.LeftToRight,
               new Typeface("Arial"),
               20,
               Brushes.Gray);
                GeometryDrawing gomb_szoveg = new GeometryDrawing(null, new Pen(Brushes.DarkMagenta, 2), formattedText_Gomb_Szoveg.BuildGeometry(new System.Windows.Point(390, 160)));
                dg.Children.Add(gomb_szoveg);
            }

            GeometryDrawing gombok_teglalap = new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.GombModul_Teglalap));

            ImageDrawing simon0 = new ImageDrawing();
            simon0.Rect = this.model.SimonModul_Negyzetek[0];
            simon0.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/red-button.png", UriKind.Relative));
            dg.Children.Add(simon0);

            ImageDrawing simon1 = new ImageDrawing();
            simon1.Rect = this.model.SimonModul_Negyzetek[1];
            simon1.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/yellow-button.png", UriKind.Relative));
            dg.Children.Add(simon1);

            ImageDrawing simon2 = new ImageDrawing();
            simon2.Rect = this.model.SimonModul_Negyzetek[2];
            simon2.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/blue-button.png", UriKind.Relative));
            dg.Children.Add(simon2);

            ImageDrawing simon3 = new ImageDrawing();
            simon3.Rect = this.model.SimonModul_Negyzetek[3];
            simon3.ImageSource = new BitmapImage(
                new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/green-button.png", UriKind.Relative));
            dg.Children.Add(simon3);

            if (this.model.SimonModul_IsLevelDone[0] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[0]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[0]));

                dg.Children.Add(jelzes);
            }

            if (this.model.SimonModul_IsLevelDone[1] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[1]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[1]));

                dg.Children.Add(jelzes);
            }

            if (this.model.SimonModul_IsLevelDone[2] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[2]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.SimonModul_LevelTeglalap[2]));

                dg.Children.Add(jelzes);
            }

            if (this.model.SimonModul_Vilagit[0])
            {
                GeometryDrawing simonkeret = new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.GreenYellow, 7), new RectangleGeometry(this.model.SimonModul_Negyzetek[0]));
                dg.Children.Add(simonkeret);
            }
            else if (this.model.SimonModul_Vilagit[1])
            {
                GeometryDrawing simonkeret = new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.GreenYellow, 7), new RectangleGeometry(this.model.SimonModul_Negyzetek[1]));
                dg.Children.Add(simonkeret);
            }
            else if (this.model.SimonModul_Vilagit[2])
            {
                GeometryDrawing simonkeret = new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.GreenYellow, 7), new RectangleGeometry(this.model.SimonModul_Negyzetek[2]));

                dg.Children.Add(simonkeret);
            }
            else if (this.model.SimonModul_Vilagit[3])
            {
                GeometryDrawing simonkeret = new GeometryDrawing(Brushes.Transparent, new Pen(Brushes.GreenYellow, 7), new RectangleGeometry(this.model.SimonModul_Negyzetek[3]));

                dg.Children.Add(simonkeret);
            }

            for (int i = 0; i < this.model.VezetekekSet.Length; i++)
            {
                if (this.model.VezetekekSet[i] == 'F')
                {
                    ImageDrawing fekete = new ImageDrawing();
                    fekete.Rect = this.model.VezetekekModul_Vezetekek[i];
                    fekete.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/fekete.png", UriKind.Relative));
                    dg.Children.Add(fekete);
                }
                else if (this.model.VezetekekSet[i] == 'S')
                {
                    ImageDrawing sarga = new ImageDrawing();
                    sarga.Rect = this.model.VezetekekModul_Vezetekek[i];
                    sarga.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/sarga.png", UriKind.Relative));
                    dg.Children.Add(sarga);
                }
                else if (this.model.VezetekekSet[i] == 'W')
                {
                    ImageDrawing feher = new ImageDrawing();
                    feher.Rect = this.model.VezetekekModul_Vezetekek[i];
                    feher.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/feher.png", UriKind.Relative));
                    dg.Children.Add(feher);
                }
                else if (this.model.VezetekekSet[i] == 'Z')
                {
                    ImageDrawing zold = new ImageDrawing();
                    zold.Rect = this.model.VezetekekModul_Vezetekek[i];
                    zold.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/zold.png", UriKind.Relative));
                    dg.Children.Add(zold);
                }
                else if (this.model.VezetekekSet[i] == 'P')
                {
                    ImageDrawing piros = new ImageDrawing();
                    piros.Rect = this.model.VezetekekModul_Vezetekek[i];
                    piros.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/piros.png", UriKind.Relative));
                    dg.Children.Add(piros);
                }
                else if (this.model.VezetekekSet[i] == 'K')
                {
                    ImageDrawing kek = new ImageDrawing();
                    kek.Rect = this.model.VezetekekModul_Vezetekek[i];
                    kek.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/kek.png", UriKind.Relative));
                    dg.Children.Add(kek);
                }
                else if (this.model.VezetekekSet[i] == 'Ü')
                {
                    this.model.VezetekekModul_Vezetekek[i] = new Rect(0, 0, 0, 0);
                }
            }

            for (int i = 0; i < this.model.VezetekekSet.Length; i++)
            {
                if (this.model.VezetekekSet[i] == 'F' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing fekete = new ImageDrawing();
                    fekete.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    fekete.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/fekete_elvagott.png", UriKind.Relative));
                    dg.Children.Add(fekete);
                }
                else if (this.model.VezetekekSet[i] == 'S' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing sarga = new ImageDrawing();
                    sarga.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    sarga.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/sarga_elvagott.png", UriKind.Relative));
                    dg.Children.Add(sarga);
                }
                else if (this.model.VezetekekSet[i] == 'W' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing feher = new ImageDrawing();
                    feher.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    feher.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/feher_elvagott.png", UriKind.Relative));
                    dg.Children.Add(feher);
                }
                else if (this.model.VezetekekSet[i] == 'Z' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing zold = new ImageDrawing();
                    zold.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    zold.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/zold_elvagott.png", UriKind.Relative));
                    dg.Children.Add(zold);
                }
                else if (this.model.VezetekekSet[i] == 'P' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing piros = new ImageDrawing();
                    piros.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    piros.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/piros_elvagott.png", UriKind.Relative));
                    dg.Children.Add(piros);
                }
                else if (this.model.VezetekekSet[i] == 'K' && this.model.IsElvagott[i] == true)
                {
                    ImageDrawing kek = new ImageDrawing();
                    kek.Rect = this.model.VezetekekModul_Vezetekek_Elvagott[i];
                    kek.ImageSource = new BitmapImage(
                        new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/kek_elvagott.png", UriKind.Relative));
                    dg.Children.Add(kek);
                }
                else if (this.model.VezetekekSet[i] == 'Ü' && this.model.IsElvagott[i] == true)
                {
                    this.model.VezetekekModul_Vezetekek[i] = new Rect(0, 0, 0, 0);
                }
            }

            dg.Children.Add(oraJelzo);
            dg.Children.Add(betusGombJelzo);
            dg.Children.Add(gombJelzo);
            dg.Children.Add(simonJelzo);
            dg.Children.Add(memoriaJelzo);
            dg.Children.Add(vezetekJelzo);
            dg.Children.Add(hatralevoIdo);

            dg.Children.Add(gombok_teglalap);

            for (int i = 0; i < this.model.BetusGombModul_Gombok.Length; i++)
            {
                ImageDrawing gomb = new ImageDrawing();
                gomb.Rect = this.model.BetusGombModul_Gombok[i];
                gomb.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/white-button.png", UriKind.Relative));
                dg.Children.Add(gomb);
                if (this.model.BetusGombModul_Correct[i] == true)
                {
                    GeometryDrawing keret = new GeometryDrawing(Brushes.Transparent,
                        new Pen(Brushes.GreenYellow, 4),
                        new RectangleGeometry(this.model.BetusGombModul_Gombok[i]));

                    dg.Children.Add(keret);
                }
            }

            dg.Children.Add(betusGomb_betu0);
            dg.Children.Add(betusGomb_betu1);
            dg.Children.Add(betusGomb_betu2);
            dg.Children.Add(betusGomb_betu3);

            for (int i = 0; i < this.model.MemoriaModul_SzamosTeglalapok.Length; i++)
            {
                ImageDrawing gomb = new ImageDrawing();
                gomb.Rect = this.model.MemoriaModul_SzamosTeglalapok[i];
                gomb.ImageSource = new BitmapImage(
                    new Uri(Directory.GetParent(workingDirectory).Parent.FullName + @"/Image/white-button.png", UriKind.Relative));

                dg.Children.Add(gomb);
            }

            if (this.model.Memoria_IsLevelDone[0] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[0]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[0]));

                dg.Children.Add(jelzes);
            }

            if (this.model.Memoria_IsLevelDone[1] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[1]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[1]));

                dg.Children.Add(jelzes);
            }

            if (this.model.Memoria_IsLevelDone[2] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[2]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[2]));

                dg.Children.Add(jelzes);
            }

            if (this.model.Memoria_IsLevelDone[4] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[4]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[4]));

                dg.Children.Add(jelzes);
            }

            if (this.model.Memoria_IsLevelDone[3] == false)
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[3]));

                dg.Children.Add(jelzes);
            }
            else
            {
                GeometryDrawing jelzes = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new RectangleGeometry(this.model.MemoriaModul_KisTeglalapok[3]));

                dg.Children.Add(jelzes);
            }

            dg.Children.Add(memoria_szam0);
            dg.Children.Add(memoria_szam1);
            dg.Children.Add(memoria_szam2);
            dg.Children.Add(memoria_szam3);
            dg.Children.Add(memoria_felsoSzam);

            dg.Children.Add(serialNumber);

            if (this.model.OraModul_IsDone == true)
            {
                GeometryDrawing oraJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.OraModul_Jelzofeny_Zold));
                dg.Children.Add(oraJelzoZold);
            }

            if (this.model.BetusGombModul_IsDone == true)
            {
                GeometryDrawing betusGombJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.BetusGombModul_Jelzofeny_Zold));
                dg.Children.Add(betusGombJelzoZold);
            }

            if (this.model.GombModul_IsDone == true)
            {
                GeometryDrawing gombJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.GombModul_Jelzofeny_Zold));
                dg.Children.Add(gombJelzoZold);
            }

            if (this.model.SimonModul_IsDone == true)
            {
                GeometryDrawing simonJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.SimonModul_Jelzofeny_Zold));
                dg.Children.Add(simonJelzoZold);
            }

            if (this.model.MemoriaModul_IsDone == true)
            {
                GeometryDrawing memoriaJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.MemoriaModul_Jelzofeny_Zold));
                dg.Children.Add(memoriaJelzoZold);
            }

            if (this.model.VezetekekModul_IsDone == true)
            {
                GeometryDrawing vezetekJelzoZold = new GeometryDrawing(Brushes.LightGreen, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.VezetekekModul_Jelzofeny_Zold));
                dg.Children.Add(vezetekJelzoZold);
            }

            if (this.model.OraModul_IsDone)
            {
                GeometryDrawing oraJelzoKesz = new GeometryDrawing(Brushes.Green, new Pen(Brushes.Black, 1), new EllipseGeometry(this.model.OraModul_Jelzofeny_Zold));
            }

            if (this.model.OraModul_HatralevoIdo == 0 || this.model.Hiba == true)
            {
                dg.Children.Add(robbanas);
            }

            context.DrawDrawing(dg);
        }
    }
}
